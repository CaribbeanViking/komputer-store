/* Get elements */
const balanceElement = document.getElementById("balance");
const getLoanElement = document.getElementById("get-loan");
const debtElement = document.getElementById("debt");
const debtTextElement = document.getElementById("debt-text");
const payElement = document.getElementById("pay");
const bankElement = document.getElementById("bank");
const workElement = document.getElementById("work");
const repayElement = document.getElementById("repay");
const laptopElement = document.getElementById("laptop");
const descriptionElement = document.getElementById("descr");
const laptopsElement = document.getElementById("laptops");
const ulElement = document.getElementById("ul-laptops");
const priceElement = document.getElementById("price");
const buyElement = document.getElementById("buy-button");
const imageElement = document.getElementById("image");

/* New instances of classes */
const bank = new Bank();
const worker = new Work();

/* Set initial info */
updateInfo(); // Set initial HTML-element states
hideAndReset(); // Hide and reset values on start

/* Actual laptop */
let laptopPrice = 0;

/* Fetch API info */
let laptops = [];
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (laptops = data))
  .then((laptops) => addLaptopsToList(laptops));

const addLaptopsToList = (laptops) => {
  laptops.forEach((x) => addLaptopToList(x));
};

const addLaptopToList = (laptop) => {
  const laptopElement = document.createElement("option");
  laptopElement.value = laptop.id;
  laptopElement.appendChild(document.createTextNode(laptop.title));
  laptopsElement.appendChild(laptopElement);
};

/* Event handler for select element */
const handleLaptopChange = (e) => {
  ulElement.innerText = ""; // Reset unordered list
  imageElement.style.display = "initial";
  const selectedLaptop = laptops[e.target.selectedIndex - 1];
  laptopElement.innerText = selectedLaptop.title;
  descriptionElement.innerText = selectedLaptop.description;
  priceElement.innerText = selectedLaptop.price + " kr";
  laptopPrice = selectedLaptop.price;

  // Loop to get specs
  for (let i in selectedLaptop.specs) {
    const listElement = document.createElement("li");
    listElement.appendChild(document.createTextNode(selectedLaptop.specs[i]));
    ulElement.appendChild(listElement);
  }

  // Get images
  const image =
    "https://noroff-komputer-store-api.herokuapp.com/" + selectedLaptop.image;
  imageElement.src = image;
};

laptopsElement.addEventListener("change", handleLaptopChange);

/* Add event listener to 'Get Loan' button */
getLoanElement.addEventListener("click", promptGetLoan);
/* Function for 'Get Loan' button */
function promptGetLoan() {
  let input = prompt("Please enter loan amount."); // Prompt
  input = parseInt(input);

  if (bank.nrOfLoans != 0) {
    // If there's already a loan
    alert("Sort out your economy, you can't have more than one loan.");
  } else if (input > 2 * bank.balance) {
    // Loan criteria
    alert("Sorry, you cannot get a loan more than twice your balance.");
  } else {
    bank.balance += input; // Increment balance
    bank.debt += input; // Increment debt
    bank.nrOfLoans++;
    debtElement.classList.remove("hide"); // Show debt related info
    debtTextElement.classList.remove("hide");
    repayElement.classList.remove("hide");
    updateInfo(); // Update HTML-elements
  }
}

/* Add event listener to 'Bank' button  */
bankElement.addEventListener("click", bankTransfer);
/* Bank transfer function */
function bankTransfer() {
  if (bank.debt > 0 && worker.salary * 0.1 < bank.debt) {
    // If there's a debt and bank amount is less than debt..
    bank.debt -= worker.salary * 0.1; // 10% towards debt
    bank.balance += worker.salary * 0.9; // Save 90% of salary
    worker.salary = 0; // Reset salary
    updateInfo(); // Update HTML-elements
    if (bank.debt === 0) {
      hideAndReset();
    } // Hide debt related info
  } else if (bank.debt > 0 && worker.salary * 0.1 >= bank.debt) {
    // If there's a debt and bank amount is more/equal than debt..
    bank.balance += worker.salary - bank.debt; // Increment left over
    bank.debt = 0; // Debt paid of
    worker.salary = 0; // Reset salary
    updateInfo(); // Update HTML-elements
    hideAndReset(); // Hide debt related info and set nrOfLoans to 0.
  } else {
    bank.balance += worker.salary; // Bank all salary
    worker.salary = 0; // Reset salary
    updateInfo(); // Update HTML-elements
  }
}

/* Add event listener to 'work' button */
workElement.addEventListener("click", work);
/* Work function */
function work() {
  worker.salary = worker.salary + 100; // Increment pay by 100
  updateInfo(); // Update HTML-elements
}

/* Add event listener to 'repay' button */
repayElement.addEventListener("click", repay);
/* Repay function */
function repay() {
  if (worker.salary >= bank.debt) {
    // If salary is larger/equal than debt
    bank.balance += worker.salary - bank.debt; // Bank left over salary
    bank.debt = 0; // Debt paid of
    worker.salary = 0; // Reset salary
    updateInfo(); // Update HTML-elements
    hideAndReset(); // Hide debt related info and set nrOfLoans to 0.
  } else {
    bank.debt -= worker.salary; // All salary towards debt
    worker.salary = 0; // Reset salary
    updateInfo(); // Update HTML-elements
  }
}

buyElement.addEventListener("click", buy);
/* Buy function */
function buy() {
  if (laptopPrice === 0) {
    alert("Choose a laptop first.");
  } else if (laptopPrice > bank.balance) { // If laptop is to expensive..
    alert("Sorry, you can't afford this laptop.");
    updateInfo();
  } else {
    bank.balance -= laptopPrice;
    alert("Congratulations, you are the owner of a new laptop!");
    updateInfo(); // Update HTML-elements
  }
  laptopPrice = 0;
}

/* Function to update HTML-elements */
function updateInfo() {
  debtElement.innerText = bank.debt + " kr";
  balanceElement.innerText = bank.balance + " kr";
  payElement.innerText = worker.salary + " kr";
  laptopsElement.selectedIndex = null;
  priceElement.innerText = "Price";
  ulElement.innerText = "";
  laptopElement.innerText = "Laptop";
  descriptionElement.innerText = "Description";
  imageElement.style.display = "none";
}

/* Function that hides debt related info and set nrOfLoans to 0. */
function hideAndReset() {
  bank.nrOfLoans = 0;
  repayElement.classList.add("hide");
  debtElement.classList.add("hide");
  debtTextElement.classList.add("hide");
}
